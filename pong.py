import pygame
from pygame.locals import *

pygame.init()
#gg=G

#LOGGER
width = 1920
height = 1080
screen = pygame.display.set_mode((width, height))
pygame.display.set_caption("Pong")
BLACK = (0, 0, 0)
GRAY = (127, 127, 127)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
CYAN = (0, 255, 255)
MAGENTA = (255, 0, 255)
MAX_SPEED = 2,5
clock = pygame.time.Clock()
PLAYER_SPEED = 15
running = True
pscore = 0
aiscore = 0

score0 = pygame.image.load("resources/0.png")
score1 = pygame.image.load("resources/1.png")
score2 = pygame.image.load("resources/2.png")
score3 = pygame.image.load("resources/3.png")
score4 = pygame.image.load("resources/4.png")
score5 = pygame.image.load("resources/5.png")
score6 = pygame.image.load("resources/6.png")
score7 = pygame.image.load("resources/7.png")
score8 = pygame.image.load("resources/8.png")
score9 = pygame.image.load("resources/9.png")


class ball:
    
    def __init__(self,x, y, color, directionx, directiony, speed):
        self.x = x
        self.y = y
        self.color = color
        self.directionx = directionx
        self.directiony = directiony
        self.speed = speed


    def move(self):
        if(self.x <= 190 and abs(player.y-self.y)<height/4):
            self.directionx = 1
            self.speed +=0.3
            if(player.thrust != 0):
                self.directiony = player.thrust
        elif(self.x >= width-110 and abs(robocop.y-self.y)<height/4):
            self.directionx = -1
            self.speed +=0.3
            if(robocop != 0):
                self.directiony = robocop.thrust
        if(self.y>height-45):
            self.directiony = -1
        elif(self.y < 45):
            self.directiony = 1



        if(self.directiony == 1):
            self.y += self.speed
        elif(self.directiony == -1):
            self.y -= self.speed
        if(self.directionx == 1):
            self.x += self.speed
        else:
            self.x -= self.speed

class stick:

    def __init__(self,x,y,color,thrust):
        self.x = x
        self.y = y
        self.color = color
        self.thrust = thrust

def moveAI():
    #down
    if(john.y > robocop.y and abs(john.y - robocop.y > 40) and robocop.y < 800):
        robocop.thrust=+1
        robocop.y += PLAYER_SPEED
    #up
    elif(john.y < robocop.y and abs(john.y - robocop.y < 40) and robocop.y > 0):
        robocop.thrust=-1
        robocop.y -= PLAYER_SPEED
    

john = ball(width/2,height/2,WHITE,1,1,11) 
player = stick(100,height/2,WHITE,0)
robocop =stick(width-100,height/2,WHITE,0)


def renderObjects():
    #john.move()
    
    if(pscore == 0):
        screen.blit(score0, (1920/3,100))
    elif(pscore == 1):
        screen.blit(score1, (1920/3,100))
    elif(pscore == 2):
        screen.blit(score2, (1920/3,100))
    elif(pscore == 3):
        screen.blit(score3, (1920/3,100))
    elif(pscore == 4):
        screen.blit(score4, (1920/3,100))
    elif(pscore == 5):
        screen.blit(score5, (1920/3,100))
    elif(pscore == 6):
        screen.blit(score6, (1920/3,100))
    elif(pscore == 7):
        screen.blit(score7, (1920/3,100))
    elif(pscore == 8):
        screen.blit(score8, (1920/3,100))    
    elif(pscore == 9):
        screen.blit(score9, (1920/3,100))            

    if(aiscore == 0):
        screen.blit(score0, (1920-1920/3,100))
    elif(aiscore == 1):
        screen.blit(score1, (1920-1920/3,100))
    elif(aiscore == 2):
        screen.blit(score2, (998,100))
    elif(aiscore == 3):
        screen.blit(score3, (1920-1920/3,100))
    elif(aiscore == 4):
        screen.blit(score4, (1920-1920/3,100))
    elif(aiscore == 5):
        screen.blit(score5, (1920-1920/3,100))
    elif(aiscore == 6):
        screen.blit(score6, (1920-1920/3,100))
    elif(aiscore == 7):
        screen.blit(score7, (1920-1920/3,100))
    elif(aiscore == 8):
        screen.blit(score8, (1920-1920/3,100))    
    elif(aiscore == 9):
        screen.blit(score9, (1920-1920/3,100))            
    pygame.draw.rect(screen,player.color,[player.x, player.y, 50, height/4],0)
    pygame.draw.rect(screen,robocop.color,[robocop.x, robocop.y, 50, height/4],0)
    pygame.draw.circle(screen, john.color, [john.x,john.y], 40) 

def checkBall():
    global aiscore
    if(john.x < 140):
        aiscore += 1
        john.x = width/2
        john.y = height/2
        john.speed = 13



while running:

    clock.tick(60)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                running = False
            elif event.key == pygame.K_w or event.key == pygame.K_UP:
                player.thrust = -1
                #player.y += PLAYER_SPEED
            elif event.key == pygame.K_s or event.key == pygame.K_DOWN:
                player.thrust = 1
                #player.y -= PLAYER_SPEED
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_w or event.key == pygame.K_UP or event.key == pygame.K_s or event.key == pygame.K_DOWN:
                player.thrust = 0

    screen.fill(BLACK)              
    moveAI()
    john.move()
    checkBall()
    #down
    if (player.thrust == 1 and player.y < 800):
        player.y += PLAYER_SPEED
        #print("pressed up: "+ player.y)
    #up
    elif(player.thrust == -1 and player.y > 0):
        player.y -= PLAYER_SPEED    
        #print("pressed down: "+ player.y)
    renderObjects()
    #print(john.x)
    pygame.display.update()
    #player.thrust = 0


pygame.quit()